// proxy http requests from https-enabled websites and send header info to the API
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/"

// call the getSquadList function when the form is submitted
$("#search-form").on("submit", e => {
  e.preventDefault();
  if (!$("#squad-search").val()) return;
  $("#squad-search").blur(); // hides keyboard on mobile
  $("footer").fadeOut(1000);
  $("#squad-list").empty();  
  getSquadList();
});

/*
 * Call the Squads Of Death API to get a list of squads for the given search term
 * Proxy sends required JSON headers and allows us to connect over HTTPS
 * Optional page argument for next/previous buttons when we get more than 20 squads
 * To Do: Timeout + fallback server in case proxy is slow or down
 * To Do: Algorithm to allow for more natural searches when special characters involved
 */
function getSquadList(page) {
  const baseURL = CORS_PROXY + "http://gsow.net/gsow/api/v1/squads?",
    params = `name=${encodeURIComponent($("#squad-search").val())}&st=squad`,
    url = page ? baseURL + params + "&" + page : baseURL + params;

  showLoader();
  $.ajaxSetup({timeout: 10000});
  $.getJSON(url, handleSquadData).fail(showError);
}

// Error handling and page animation before writing search results to the page
function handleSquadData(squadList) {
  if (squadList.data.length === 0) {
    $("#squad-list").html(
      `<p>
         No Results for <em>${$("#squad-search").val()}</em>.
         Try a shorter search to get a list of matching squads, 
         e.g. "Imperial" instead of "Imperial'Snipers"
       </p>`
    );
    return;
  }
  


  $("header").animate({ height: 0 }, 1500);
  $("main").animate({ "min-height": "100%" }, 1500);
  $("#title").hide();
  
  squadList.data.length === 1 ? getSquadDetails(squadList.data[0].id) : showSquadSearchResults(squadList);
}

// Convert JSON list of squads into a table
function showSquadSearchResults(squadList) {
  $("#squad-list").html(
    `<p class="text-center instructions">Select a squad below.</p>
      <table class="table table-bordered" id="results-table">
       <thead>
         <tr>
           <th>Squad</th>
           <th>Faction</th>
         </tr>
       </thead>
       <tbody></tbody>
     </table>`
  );

  // the squad ID is needed to call the API and get details for a selected squad
  squadList.data.forEach(squad => {
    $("#results-table tbody").append(
      `<tr id="${squad.id}">
         <td>${decodeURIComponent(squad.name)}</td>
         <td>${squad.faction}</td>
       </tr>`
    );
  });

  // select a row to get a list of squad members
  $("tbody tr").on("click", e => getSquadDetails($(e.currentTarget).attr("id")));

  // add previous/next buttons if we have more than 20 results
  if (squadList.next_page_url || squadList.prev_page_url)
    getMoreResults(squadList);
}

// Hide the previous/next buttons (if any), show a loader,
// and call the API again to get a list of the members
function getSquadDetails(id) {
  $("#next, #previous").addClass("hidden");
  $("#squad-list").empty();
  showLoader();

  // connect via proxy if we're using HTTPS, Squads of Death only supports HTTP
  const url = `${CORS_PROXY}http://gsow.net/gsow/api/v1/squads/${id}/members`;
  $.ajaxSetup({timeout: 10000});
  $.getJSON(url, showSquadMembers).fail(showError);
}

// previous/next buttons when there are more than 20 squads shown in a search result
function getMoreResults(data) {
  if (data.next_page_url) {
    $("#next")
      .removeClass("hidden")
      .unbind("click")
      .on("click", () => getSquadList(data.next_page_url.split("?")[1]));
  } else {
    $("#next").addClass("hidden");
  }

  if (data.prev_page_url) {
    $("#previous")
      .removeClass("hidden")
      .unbind("click")
      .on("click", () => getSquadList(data.prev_page_url.split("?")[1]));
  } else {
    $("#previous").addClass("hidden");
  }
}

// When we get member details back from the API, show all members in a table
// and present a button to filter selected members to a final list
function showSquadMembers(squadDetails) {

  // main table layout
  $("#squad-list").html(
    `<div class="row text-center">
       <div class="col-xs-12">
         <p class="instructions">Select your enemies and outposts</p>
         <p class="select-count">0 / 15</p>
         <button class="btn btn-success center-block filter-button">Get The Signup List</button>
       </div>
     </div>
     <div class="row outpost-selections">
       <div class="col-xs-12"></div>
     </div>
     <div class="row">
       <div class="col-xs-12">
         <table class="table table-bordered" id="results-table">
           <thead>
             <tr>
               <th>Name</th>
               <th>HQ Level</th>
               <th>Base Strength</th>
             </tr>
           </thead>
           <tbody></tbody>
         </table>
       </div>
     </div>
     <div class="row">
       <div class="col-xs-12">
         <p class="select-count">0 / 15</p>
         <button class="btn btn-success center-block filter-button">Get The Signup List</button>
       </div>
     </div>`
  );

  // add the Ouposts checkboxes
  const outposts = ["Takodana", "Tatooine", "Dandoran", "Hoth", "Yavin", "Er'Kit"];
  outposts.forEach(outpost => {
    $('.outpost-selections .col-xs-12').append(
      `<div class="form-group">
        <input type="checkbox" id="${outpost}">
        <label for="${outpost}">${outpost}</label>
      </div>`
    )
  });
  
  // populate each table row
  // give each row an ID corresponding to the index of each member in the JSON for filtering
  squadDetails.forEach((member, i) => {
    $("#results-table tbody").append(
      `<tr id="${i}" class="squad-member-row">
         <td>${decodeURIComponent(member.name).split("]")[1] ||
           decodeURIComponent(member.name)}</td>
         <td class="text-center">${member.hqLevel}</td>
         <td class="text-center">${member.xp}</td>
       </tr>`
    );
  });

  // highlight squad member rows when selected
  $("tbody tr").on("click", selectRow);

  // show final list when the Filter button is clicked
  $(".filter-button").on("click", e => showFilteredList(e, squadDetails));
}

// converts highighted table rows to text-based list in a <textarea> for copy/paste into chat
function showFilteredList(e, squadDetails) {
  const enemies = [],
        outposts = [];
  
  $.each($(".selected"), (i, member) => enemies.push(squadDetails[$(member).attr("id")]));
  $.each($("input[type=checkbox]:checked"), (i, outpost) => outposts.push($(outpost).attr("id")));

  $("#squad-list").empty().html(
      `<textarea id="filtered-list" readonly rows="${enemies.length +
        10}">Outposts: \n----------\n</textarea>`
    ).append(
      `<p class="text-left">
       War history for this squad is at:<br>
       <small>
         <a href="http://gsow.net/gsow/api/v1/squads/${squadDetails[0].squadId}/history">
           http://gsow.net/gsow/api/v1/squads/${squadDetails[0].squadId}/history
         </a>
       </small>
     </p>`
    );
  
  outposts.forEach(outpost => {
    if(outpost !== "Er'Kit") $("#filtered-list").append(`${outpost}: \n`);
  });
  
  let kit = outposts.includes("Er'Kit") ? " (including Er'Kit)" : "";
  $("#filtered-list").append(`Retakes${kit}: \n\nBases (HQ-Strength)\n--------------------\n`);

  enemies.forEach(member => {
    const name = decodeURIComponent(member.name).split("]")[1] || decodeURIComponent(member.name);
    $("#filtered-list").append(`${name} (${member.hqLevel}: ${member.xp}) - \n`);
  });

  // select all text when clicked
  $("#filtered-list").on("click", function() {
    $(this).focus();
    $(this)[0].setSelectionRange(0, $(this).val().length);
  });
}

// select a row to highlight it
function selectRow() {
  $(this).hasClass("selected")
    ? $(this).removeClass("selected")
    : $(this).addClass("selected");

  // show a count of how many are selected with an indicator when 15/15
  $(".select-count").html(`${$(".selected").length || 0} / 15`);

  if ($(".selected").length == 15) {
    $(".selected").css({ "background-color": "#fff" });
    $(".select-count").css("color", "#F38630");
  } else {
    $(".squad-member-row").css("background-color", "");
    $(".select-count").css("color", "");
  }
}

// Show loading text
// To Do: Add a spinner/loading progress bar
function showLoader() {
  $("#next, #previous").addClass("hidden");
  $("#squad-list").append("<p id='loading-text'>Loading ... </p>");
}

function showError(err) {
  $("#next, #previous").addClass("hidden");
  $("#squad-list").html(
    `<pre class="text-left">${JSON.stringify(err, null, 2)}</pre>`
  );
}
